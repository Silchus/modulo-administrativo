<?php

use Illuminate\Database\Seeder;
use App\Models\Localidad;

class LocalidadesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        Localidad::create(['nombre' => 'Santa Rosa','codigoPostal' => '6300', 'provincia_id' => 1]);
        Localidad::create(['nombre' => 'General Pico','codigoPostal' => '6360', 'provincia_id' => 1]);

    }
}