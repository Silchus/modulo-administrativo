<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVentasTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ventas', function (Blueprint $table) {
            $table->increments('id');
            $table->date('fecha');
            $table->text('descripcion');
            $table->integer('cantidad');
            $table->float('importe');
            $table->string('cajaDiaria');
            $table->integer('articulo_id');
            $table->float('costo');
            $table->integer('cliente_id');
            $table->string('vendedor');
            $table->string('usuario');
            $table->string('forma1');
            $table->string('forma2');
            $table->integer('facturaVenta_id');
            $table->float('iva');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ventas');
    }
}
