<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateArticulosTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articulos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('codigo');
            $table->text('descripcion');
            $table->string('marca');
            $table->integer('stock');
            $table->integer('cantidad');
            $table->integer('stockMinimo');
            $table->date('fechaVencimiento');
            $table->string('ubicacion');
            $table->float('precio1');
            $table->float('precio2');
            $table->float('precio3');
            $table->float('costo');
            $table->text('porcentajeGanancia1');
            $table->text('porcentajeGanancia2');
            $table->text('porcentajeGanancia3');
            $table->float('precio');
            $table->float('iva');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('articulos');
    }
}
