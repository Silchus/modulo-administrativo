<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCuentaCorrientesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cuenta_corrientes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('codigo');
            $table->date('fecha');
            $table->text('descripcion');
            $table->integer('cantidad');
            $table->float('importe');
            $table->string('tipo');
            $table->float('costo');
            $table->integer('articulo_id');
            $table->float('saldo');
            $table->date('ultimoPago');
            $table->date('ultimaCompra');
            $table->integer('cliente_id');
            $table->integer('recibo');
            $table->string('reciboTex');
            $table->float('costoReal');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cuenta_corrientes');
    }
}
