<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFacturaVentasTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('factura_ventas', function (Blueprint $table) {
            $table->increments('id');
            $table->date('fecha');
            $table->integer('numero');
            $table->string('codigo');
            $table->integer('cliente_id');
            $table->integer('vendedor_id');
            $table->string('forma1');
            $table->string('forma2');
            $table->float('descu');
            $table->float('totalDescu');
            $table->integer('iva_id');
            $table->float('totalSIva');
            $table->float('exento');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('factura_ventas');
    }
}
