<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | El following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted' => 'El :attribute debe ser accepted.',
    'active_url' => 'El :attribute is not a valid URL.',
    'after' => 'El :attribute debe ser a date after :date.',
    'after_or_equal' => 'El :attribute debe ser a date after or equal to :date.',
    'alpha' => 'El :attribute may only contain letters.',
    'alpha_dash' => 'El :attribute may only contain letters, números, dashes and underscores.',
    'alpha_num' => 'El :attribute may only contain letters and números.',
    'array' => 'El :attribute debe ser an array.',
    'before' => 'El :attribute debe ser a date before :date.',
    'before_or_equal' => 'El :attribute debe ser a date before or equal to :date.',
    'between' => [
        'numeric' => 'El :attribute debe ser between :min and :max.',
        'file' => 'El :attribute debe ser between :min and :max kilobytes.',
        'string' => 'El :attribute debe ser between :min and :max characters.',
        'array' => 'El :attribute must have between :min and :max items.',
    ],
    'boolean' => 'El :attribute field debe ser true or false.',
    'confirmed' => 'El :attribute confirmation does not match.',
    'date' => 'El :attribute is not a valid date.',
    'date_equals' => 'El :attribute debe ser a date equal to :date.',
    'date_format' => 'El :attribute does not match the format :format.',
    'different' => 'El :attribute and :other debe ser different.',
    'digits' => 'El :attribute debe ser :digits digits.',
    'digits_between' => 'El :attribute debe ser between :min and :max digits.',
    'dimensions' => 'El :attribute has inválido image dimensions.',
    'distinct' => 'El :attribute field has a duplicate value.',
    'email' => 'El :attribute debe ser a valid email address.',
    'exists' => 'El selected :attribute is inválido.',
    'file' => 'El :attribute debe ser a file.',
    'filled' => 'El :attribute field must have a value.',
    'gt' => [
        'numeric' => 'El :attribute debe ser greater than :value.',
        'file' => 'El :attribute debe ser greater than :value kilobytes.',
        'string' => 'El :attribute debe ser greater than :value characters.',
        'array' => 'El :attribute must have more than :value items.',
    ],
    'gte' => [
        'numeric' => 'El :attribute debe ser greater than or equal :value.',
        'file' => 'El :attribute debe ser greater than or equal :value kilobytes.',
        'string' => 'El :attribute debe ser greater than or equal :value characters.',
        'array' => 'El :attribute must have :value items or more.',
    ],
    'image' => 'El :attribute debe ser an image.',
    'in' => 'El selected :attribute is inválido.',
    'in_array' => 'El :attribute field does not exist in :other.',
    'integer' => 'El :attribute debe ser an integer.',
    'ip' => 'El :attribute debe ser a valid IP address.',
    'ipv4' => 'El :attribute debe ser a valid IPv4 address.',
    'ipv6' => 'El :attribute debe ser a valid IPv6 address.',
    'json' => 'El :attribute debe ser a valid JSON string.',
    'lt' => [
        'numeric' => 'El :attribute debe ser less than :value.',
        'file' => 'El :attribute debe ser less than :value kilobytes.',
        'string' => 'El :attribute debe ser less than :value characters.',
        'array' => 'El :attribute must have less than :value items.',
    ],
    'lte' => [
        'numeric' => 'El :attribute debe ser less than or equal :value.',
        'file' => 'El :attribute debe ser less than or equal :value kilobytes.',
        'string' => 'El :attribute debe ser less than or equal :value characters.',
        'array' => 'El :attribute must not have more than :value items.',
    ],
    'max' => [
        'numeric' => 'El :attribute may not be greater than :max.',
        'file' => 'El :attribute may not be greater than :max kilobytes.',
        'string' => 'El :attribute may not be greater than :max characters.',
        'array' => 'El :attribute may not have more than :max items.',
    ],
    'mimes' => 'El :attribute debe ser a file of type: :values.',
    'mimetypes' => 'El :attribute debe ser a file of type: :values.',
    'min' => [
        'numeric' => 'El :attribute debe ser at least :min.',
        'file' => 'El :attribute debe ser at least :min kilobytes.',
        'string' => 'El :attribute debe ser at least :min characters.',
        'array' => 'El :attribute must have at least :min items.',
    ],
    'not_in' => 'El selected :attribute is inválido.',
    'not_regex' => 'El :attribute format is inválido.',
    'numeric' => 'El :attribute debe ser a número.',
    'present' => 'El :attribute field debe ser present.',
    'regex' => 'El :attribute format is inválido.',
    'required' => 'El :attribute field is requerido.',
    'required_if' => 'El :attribute field is requerido when :other is :value.',
    'required_unless' => 'El :attribute field is requerido unless :other is in :values.',
    'required_with' => 'El :attribute field is requerido when :values is present.',
    'required_with_all' => 'El :attribute field is requerido when :values are present.',
    'required_without' => 'El :attribute field requerido requerido when :values es not present.',
    'required_without_all' => 'El :attribute field es requerido when none of :values are present.',
    'same' => 'El :attribute and :other must match.',
    'size' => [
        'numeric' => 'El :attribute debe ser :size.',
        'file' => 'El :attribute debe ser :size kilobytes.',
        'string' => 'El :attribute debe ser :size characters.',
        'array' => 'El :attribute must contain :size items.',
    ],
    'starts_with' => 'El :attribute must start with one of the following: :values',
    'string' => 'El :attribute debe ser a string.',
    'timezone' => 'El :attribute debe ser a valid zone.',
    'unique' => 'El :attribute has already been taken.',
    'uploaded' => 'El :attribute failed to upload.',
    'url' => 'El :attribute format is inválido.',
    'uuid' => 'El :attribute debe ser a valid UUID.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | El following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [],

];
