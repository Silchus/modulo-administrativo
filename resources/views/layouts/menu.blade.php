
<li class="{{ Request::is('proveedor*') ? 'active' : '' }}">
    <a href="{!! route('proveedores.index') !!}"><i class="fa fa-credit-card"></i><span>Proveedores</span></a>
</li>
<li class="{{ Request::is('articulos*') ? 'active' : '' }}">
    <a href="{!! route('articulos.index') !!}"><i class="fa fa-edit"></i><span>Articulos</span></a>
</li>

<li class="{{ Request::is('ventas*') ? 'active' : '' }}">
    <a href="{!! route('ventas.index') !!}"><i class="fa fa-edit"></i><span>Ventas</span></a>
</li>

<li class="{{ Request::is('tipoArticulos*') ? 'active' : '' }}">
    <a href="{!! route('tipoArticulos.index') !!}"><i class="fa fa-edit"></i><span>Categorias</span></a>
</li>

<li class="{{ Request::is('ivas*') ? 'active' : '' }}">
    <a href="{!! route('ivas.index') !!}"><i class="fa fa-edit"></i><span>Iva</span></a>
</li>

<li class="{{ Request::is('cuentaCorrientes*') ? 'active' : '' }}">
    <a href="{!! route('cuentaCorrientes.index') !!}"><i class="fa fa-edit"></i><span>Cuentas Corrientes</span></a>
</li>

<li class="{{ Request::is('facturaVentas*') ? 'active' : '' }}">
    <a href="{!! route('facturaVentas.index') !!}"><i class="fa fa-edit"></i><span>Factura Ventas</span></a>
</li>

