<!-- Iva Field -->
<div class="form-group col-sm-6">
    {!! Form::label('iva', 'Iva:') !!}
    {!! Form::text('iva', null, ['class' => 'form-control']) !!}
    @if($errors->has('iva'))
        <span class="invalid-feedback" role="alert" style="color: red; font-size: 9px">
            <strong>{{$errors->first('iva')}}</strong>
        </span>
    @endif
</div>

<!-- Iva Field -->
<div class="form-group col-sm-6">
    {!! Form::label('descripcion', 'Descripción:') !!}
    {!! Form::text('descripcion', null, ['class' => 'form-control']) !!}
    @if($errors->has('descripcion'))
        <span class="invalid-feedback" role="alert" style="color: red; font-size: 9px">
            <strong>{{$errors->first('descripcion')}}</strong>
        </span>
    @endif
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('ivas.index') !!}" class="btn btn-default">Cancelar</a>
</div>
