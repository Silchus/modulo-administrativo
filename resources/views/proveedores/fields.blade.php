<!-- Nombre Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nombre', 'Nombre:') !!}
    {!! Form::text('nombre', null, ['class' => 'form-control']) !!}
    @if($errors->has('nombre'))
        <span class="invalid-feedback" role="alert" style="color: red; font-size: 9px">
            <strong>{{$errors->first('nombre')}}</strong>
        </span>
    @endif
</div>

<!-- Empresa Field -->
<div class="form-group col-sm-6">
    {!! Form::label('empresa', 'Empresa:') !!}
    {!! Form::text('empresa', null, ['class' => 'form-control']) !!}
    @if($errors->has('empresa'))
        <span class="invalid-feedback" role="alert" style="color: red; font-size: 9px">
            <strong>{{$errors->first('empresa')}}</strong>
        </span>
    @endif
</div>

<!-- Direccion Field -->
<div class="form-group col-sm-3">
    {!! Form::label('direccion', 'Direccion:') !!}
    {!! Form::text('direccion', null, ['class' => 'form-control']) !!}
    @if($errors->has('direccion'))
        <span class="invalid-feedback" role="alert" style="color: red; font-size: 9px">
            <strong>{{$errors->first('direccion')}}</strong>
        </span>
    @endif
</div>

<!-- Telefono Field -->
<div class="form-group col-sm-3">
    {!! Form::label('telefono', 'Telefono:') !!}
    {!! Form::text('telefono', null, ['class' => 'form-control']) !!}
    @if($errors->has('telefono'))
        <span class="invalid-feedback" role="alert" style="color: red; font-size: 9px">
            <strong>{{$errors->first('telefono')}}</strong>
        </span>
    @endif
</div>


<!-- Cuit Field -->
<div class="form-group col-sm-3">
    {!! Form::label('cuit', 'Cuit:') !!}
    {!! Form::text('cuit', null, ['class' => 'form-control']) !!}
    @if($errors->has('cuit'))
        <span class="invalid-feedback" role="alert" style="color: red; font-size: 9px">
            <strong>{{$errors->first('cuit')}}</strong>
        </span>
    @endif
</div>

<!-- Email Field -->
<div class="form-group col-sm-3">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::text('email', null, ['class' => 'form-control']) !!}
    @if($errors->has('email'))
        <span class="invalid-feedback" role="alert" style="color: red; font-size: 9px">
            <strong>{{$errors->first('email')}}</strong>
        </span>
    @endif
</div>

<!-- Provincia Id Field -->
<div class="form-group col-sm-3">
    {!! Form::label('provincia_id', 'Provincia:') !!}
    {!! Form::select('provincia_id', $provincias, null, ['class' => 'form-control']) !!}
    @if($errors->has('provincia_id'))
        <span class="invalid-feedback" role="alert" style="color: red; font-size: 9px">
            <strong>{{$errors->first('provincia_id')}}</strong>
        </span>
    @endif
</div>

<!-- Localidad Id Field -->
<div class="form-group col-sm-3">
    {!! Form::label('localidad_id', 'Localidad:') !!}
    {!! Form::select('localidad_id', $localidades, null, ['class' => 'form-control']) !!}
    @if($errors->has('localidad_id'))
        <span class="invalid-feedback" role="alert" style="color: red; font-size: 9px">
            <strong>{{$errors->first('localidad_id')}}</strong>
        </span>
    @endif
</div>


<!-- Saldo Field -->
<div class="form-group col-sm-3">
    {!! Form::label('saldo', 'Saldo:') !!}
    {!! Form::text('saldo', null, ['class' => 'form-control']) !!}
    @if($errors->has('saldo'))
        <span class="invalid-feedback" role="alert" style="color: red; font-size: 9px">
            <strong>{{$errors->first('saldo')}}</strong>
        </span>
    @endif
</div>

<div class="form-group col-sm-3">
</div>

<!-- Otros Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('otros', 'Otros:') !!}
    {!! Form::textarea('otros', null, ['class' => 'form-control']) !!}
    @if($errors->has('otros'))
        <span class="invalid-feedback" role="alert" style="color: red; font-size: 9px">
            <strong>{{$errors->first('otros')}}</strong>
        </span>
    @endif
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('proveedores.index') !!}" class="btn btn-default">Cancelar</a>
</div>
