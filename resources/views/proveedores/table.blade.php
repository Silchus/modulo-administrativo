<table class="table table-responsive" id="proveedores-table">
    <thead>
        <tr>
            <th>Nombre</th>
        <th>Empresa</th>
        <th>Direccion</th>
        <th>Telefono</th>
        <th>Email</th>
        <th>Saldo</th>
        <th colspan="3">Acciones</th>
        </tr>
    </thead>
    <tbody>
    @foreach($proveedores as $proveedor)
        <tr>
            <td>{!! $proveedor->nombre !!}</td>
            <td>{!! $proveedor->empresa !!}</td>
            <td>{!! $proveedor->direccion !!}</td>
            <td>{!! $proveedor->telefono !!}</td>
            <td>{!! $proveedor->email !!}</td>
            <td>{!! $proveedor->saldo !!}</td>
            <td>
                {!! Form::open(['route' => ['proveedores.destroy', $proveedor->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('proveedores.show', [$proveedor->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('proveedores.edit', [$proveedor->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>