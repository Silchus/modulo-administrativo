<!-- Id Field -->
<!--<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $proveedor->id !!}</p>
</div>-->

<!-- Nombre Field -->
<div class="form-group col-sm-3">
    {!! Form::label('Nombre', 'Nombre:') !!}
    <p>{!! $proveedor->nombre !!}</p>
</div>

<!-- Empresa Field -->
<div class="form-group col-sm-3">
    {!! Form::label('Empresa', 'Empresa:') !!}
    <p>{!! $proveedor->empresa !!}</p>
</div>

<!-- Direccion Field -->
<div class="form-group col-sm-3">
    {!! Form::label('Direccion', 'Direccion:') !!}
    <p>{!! $proveedor->direccion !!}</p>
</div>

<!-- Telefono Field -->
<div class="form-group col-sm-3">
    {!! Form::label('Telefono', 'Telefono:') !!}
    <p>{!! $proveedor->telefono !!}</p>
</div>

<!-- Localidad Id Field -->
<div class="form-group col-sm-3">
    {!! Form::label('Localidad', 'Localidad:') !!}
    <p>{!! $proveedor->localidad_id !!}</p>
</div>

<!-- Provincia Id Field -->
<div class="form-group col-sm-3">
    {!! Form::label('Provincia', 'Provincia:') !!}
    <p>{!! $proveedor->provincia_id !!}</p>
</div>

<!-- Cuit Field -->
<div class="form-group col-sm-3" >
    {!! Form::label('Cuit', 'Cuit:') !!}
    <p>{!! $proveedor->cuit !!}</p>
</div>

<!-- Email Field -->
<div class="form-group col-sm-3">
    {!! Form::label('email', 'Email:') !!}
    <p>{!! $proveedor->email !!}</p>
</div>

<!-- Otros Field -->
<div class="form-group col-sm-3">
    {!! Form::label('otros', 'Otros:') !!}
    <p>{!! $proveedor->otros !!}</p>
</div>

<!-- Saldo Field -->
<div class="form-group col-sm-3">
    {!! Form::label('saldo', 'Saldo:') !!}
    <p>{!! $proveedor->saldo !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group col-sm-3">
    {!! Form::label('created_at', 'Creado:') !!}
    <p>{!! $proveedor->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group col-sm-3">
    {!! Form::label('updated_at', 'Actualizado:') !!}
    <p>{!! $proveedor->updated_at !!}</p>
</div>

