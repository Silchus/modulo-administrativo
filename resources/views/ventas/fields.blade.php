

<!-- Main content -->
<section class="content">

<form class="form-horizontal create_sales" role="form" method="POST" action="{{ url('/venta/store') }}">

<div class="box-body">

<div class="box box-default">
    <div class="box-body">
        <div class="row">
            <div class="col-sm-3">
                <div class="form-group">
                    <label>Nombre Cliente</label>
                    <input type="text" class="form-control search_customer_name" placeholder="Buscar ..." name="socio_nombre">
                    <span class="help-block search_customer_name_empty" style="display: none;">No hay resultados...</span>

                    <span class="help-block search_purchase_category_name_empty" style="display: none;">No hay resultados ...</span>
                    <input type="hidden" class="search_customer_id" name="socio_id">
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <label>Dirección</label><br>
                    <input type="text" class="form-control search_customer_address" name="socio_direccion">
                </div>
            </div>

            <div class="col-sm-3">
                <div class="form-group">
                    <label>Teléfono</label><br>
                    <input type="text" class="form-control search_customer_contact1" name="socio_telefono">
                </div>
            </div>

          <!--  <div class="col-sm-2">
                <div class="form-group">
                    <label>Opening Balance</label><br>
                    <input type="text" name="opening_balance" class="form-control opening_balance" readonly="">
                </div>
            </div>-->

            <div class="col-sm-3">
                <div class="form-group">
                    <label>Saldo</label><br>
                    <input type="text" name="saldo" class="form-control opening_due" readonly="">
                </div>
            </div>

        </div>
    </div>
</div>

<div class="box box-default">

    <div class="box-body">

        <table class="table table-striped">
            <thead>
            <tr>
                <th>Artículo</th>
                <th>Costo</th>
                <th>Precio Publ.</th>
                <th>Cantidad</th>
                <th>Sub-Total</th>
            </tr>
            </thead>
            <tbody class="sales_container">
            <tr>
                <td>
                    <input type="text" class="form-control search_purchase_category_name" placeholder="Buscar..." name="articulo_nombre[]" autocomplete="off">
                    <span class="help-block search_purchase_category_name_empty glyphicon" style="display: none;"> No hay resultados </span>
                    <input type="hidden" class="search_category_id" name="articulo_id[]">
                </td>
            <!--    <td width="250px">
                    <select class="form-control stock_id" name="articulo_id[]">
                        <option selected="" disabled="" value="">Seleccionar</option>
                    </select>
                    <span class="search_stock_quantity"></span>
                </td>-->
                <td width="200px">
                    <input type="text" class="form-control search_purchase_cost" name="precio_compra[]" readonly="">
                </td>
                <td width="150px">
                    <input type="text" class="form-control search_selling_cost" name="precio_venta[]" >
                </td>

                <td width="50px">
                    <input type="hidden" class="search_stock_quantity" name="opening_stock[]">
                    <input type="hidden" class="closing_stock" name="closing_stock[]">

                    <input type="number" class="form-control change_sales_quantity" name="cantidad[]" min="1">
                    <small class="help-block max_stock" style="display: none;">Stock Insuficiente</small>
                </td>

                <td width="100px">
                    <input type="text" class="form-control stock_total" name="sub_total[]"  readonly="">
                </td>

                <td><button type="button" class="btn btn-danger remove_tr">&times;</button></td>
            </tr>
            </tbody>
            <tfoot>
            <tr>
                <td colspan="3">
                    <button type="button" class="btn btn-primary add_sales_product"><i class="fa fa-plus"></i> Agregar</button>
                </td>
                <td></td>
            </tr>
            </tfoot>
        </table>

        <div class="row">
            <div class="col-md-offset-8 col-md-4">
                <div class="form-group">
                    <label>Total Ventas</label><br>
                    <input type="text" class="form-control sales_total" readonly="" name="total">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-offset-4 col-md-4">
                <div class="form-group">
                    <label>Descontar ( % )</label><br>
                    <input type="number" class="form-control sales_discount_percent" name="discount_percent" step="0.01" min="0" max="100" value="0">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>Descontar ( $ )</label><br>
                    <input type="text" class="form-control sales_discount_amount" name="discount_amount" step="0.01" min="0" value="0">
                </div>
            </div>
        </div>

        <!--<div class="row">

            <div class="col-md-4">
                <div class="form-group">
                    <label>Tax Description</label><br>
                    <input type="text" class="form-control" name="tax_description">
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <label>Tax ( % )</label><br>
                    <input type="number" class="form-control sales_tax_percent" name="tax_percent"  step="0.01" min="0" max="100" value="0">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>Tax ( Amount )</label><br>
                    <input type="text" class="form-control sales_tax_amount" name="tax_amount"   step="0.01" min="0" value="0">
                </div>
            </div>
        </div>-->

    </div>
</div>

<div class="box box-default">
    <div class="box-body">
        <div class="row">

            <div class="col-sm-2">
                <div class="form-group">
                    <label>Descripción</label><br>
                    <textarea class="form-control" style="height: 35px;" name="sales_description"></textarea>
                </div>
            </div>

            <div class="col-sm-2">
                <div class="form-group">
                    <label>Importe Total</label><br>
                    <input type="text" class="form-control grand_total" name="venta_total" readonly="">
                </div>
            </div>

            <div class="col-sm-2">
                <div class="form-group">
                    <label>Pago</label><br>
                    <input type="text" class="form-control purchase_payment" name="pago">
                </div>
            </div>

            <!--<div class="col-sm-2">
                <div class="form-group">
                    <label>Closing Balance</label><br>
                    <input type="text" class="form-control closing_balance" name="closing_balance" readonly="">
                </div>
            </div>-->

            <div class="col-sm-2">
                <div class="form-group">
                    <label>Saldo Restante</label><br>
                    <input type="text" class="form-control closing_due" name="saldo" readonly="">
                </div>
            </div>

            <div class="col-sm-2">
                <div class="form-group">
                    <label>Forma de pago</label>
                    <select class="form-control" name="mode">
                        <option value="1">Efectivo</option>
                        <option value="2">Cheque</option>
                        <option value="3">Tarjeta</option>
                    </select>
                </div>
            </div>

        </div>
    </div>
</div>

</div>
<!-- /.box-body -->

<div class="box-footer">
    <button type="reset" class="btn btn-danger pull-left">Resetear</button>
    <button type="submit" class="btn btn-primary pull-right form_submit"><i class="fa fa-plus"></i> Agregar</button>
</div>
</form>

</section>
<!-- /.content -->
