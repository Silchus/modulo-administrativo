<table class="table table-responsive" id="ventas-table">
    <thead>
        <tr>
            <th>Fecha</th>
            <th>Cliente</th>
            <th>Articulo</th>
            <th>Cantidad</th>
            <th>Importe</th>
        <th>Forma1</th>
        <th>Forma2</th>
        <th>Facturaventa Id</th>
        <th colspan="3">Acciones</th>
        </tr>
    </thead>
    <tbody>
    @foreach($ventas as $venta)
        <tr>
            <td>{!! $venta->fecha !!}</td>
            <td>{!! $venta->cliente_id !!}</td>
            <td>{!! $venta->articulo_id !!}</td>
            <td>{!! $venta->cantidad !!}</td>
            <td>{!! $venta->importe !!}</td>
            <td>{!! $venta->forma1 !!}</td>
            <td>{!! $venta->forma2 !!}</td>
            <td>{!! $venta->facturaVenta_id !!}</td>
            <td>
                {!! Form::open(['route' => ['ventas.destroy', $venta->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ventas.show', [$venta->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ventas.edit', [$venta->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>