<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $venta->id !!}</p>
</div>

<!-- Fecha Field -->
<div class="form-group">
    {!! Form::label('fecha', 'Fecha:') !!}
    <p>{!! $venta->fecha !!}</p>
</div>

<!-- Descripcion Field -->
<div class="form-group">
    {!! Form::label('descripcion', 'Descripcion:') !!}
    <p>{!! $venta->descripcion !!}</p>
</div>

<!-- Cantidad Field -->
<div class="form-group">
    {!! Form::label('cantidad', 'Cantidad:') !!}
    <p>{!! $venta->cantidad !!}</p>
</div>

<!-- Importe Field -->
<div class="form-group">
    {!! Form::label('importe', 'Importe:') !!}
    <p>{!! $venta->importe !!}</p>
</div>

<!-- Cajadiaria Field -->
<div class="form-group">
    {!! Form::label('cajaDiaria', 'Cajadiaria:') !!}
    <p>{!! $venta->cajaDiaria !!}</p>
</div>

<!-- Articulo Id Field -->
<div class="form-group">
    {!! Form::label('articulo_id', 'Articulo Id:') !!}
    <p>{!! $venta->articulo_id !!}</p>
</div>

<!-- Costo Field -->
<div class="form-group">
    {!! Form::label('costo', 'Costo:') !!}
    <p>{!! $venta->costo !!}</p>
</div>

<!-- Cliente Id Field -->
<div class="form-group">
    {!! Form::label('cliente_id', 'Cliente Id:') !!}
    <p>{!! $venta->cliente_id !!}</p>
</div>

<!-- Vendedor Field -->
<div class="form-group">
    {!! Form::label('vendedor', 'Vendedor:') !!}
    <p>{!! $venta->vendedor !!}</p>
</div>

<!-- Usuario Field -->
<div class="form-group">
    {!! Form::label('usuario', 'Usuario:') !!}
    <p>{!! $venta->usuario !!}</p>
</div>

<!-- Forma1 Field -->
<div class="form-group">
    {!! Form::label('forma1', 'Forma1:') !!}
    <p>{!! $venta->forma1 !!}</p>
</div>

<!-- Forma2 Field -->
<div class="form-group">
    {!! Form::label('forma2', 'Forma2:') !!}
    <p>{!! $venta->forma2 !!}</p>
</div>

<!-- Facturaventa Id Field -->
<div class="form-group">
    {!! Form::label('facturaVenta_id', 'Facturaventa Id:') !!}
    <p>{!! $venta->facturaVenta_id !!}</p>
</div>

<!-- Iva Field -->
<div class="form-group">
    {!! Form::label('iva', 'Iva:') !!}
    <p>{!! $venta->iva !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $venta->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $venta->updated_at !!}</p>
</div>

