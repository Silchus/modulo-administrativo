<table class="table table-responsive" id="facturaVentas-table">
    <thead>
        <tr>
            <th>Fecha</th>
        <th>Numero</th>
        <th>Codigo</th>
        <th>Cliente Id</th>
        <th>Vendedor Id</th>
        <th>Forma1</th>
        <th>Forma2</th>
        <th>Descu</th>
        <th>Totaldescu</th>
        <th>Iva Id</th>
        <th>Totalsiva</th>
        <th>Exento</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($facturaVentas as $facturaVenta)
        <tr>
            <td>{!! $facturaVenta->fecha !!}</td>
            <td>{!! $facturaVenta->numero !!}</td>
            <td>{!! $facturaVenta->codigo !!}</td>
            <td>{!! $facturaVenta->cliente_id !!}</td>
            <td>{!! $facturaVenta->vendedor_id !!}</td>
            <td>{!! $facturaVenta->forma1 !!}</td>
            <td>{!! $facturaVenta->forma2 !!}</td>
            <td>{!! $facturaVenta->descu !!}</td>
            <td>{!! $facturaVenta->totalDescu !!}</td>
            <td>{!! $facturaVenta->iva_id !!}</td>
            <td>{!! $facturaVenta->totalSIva !!}</td>
            <td>{!! $facturaVenta->exento !!}</td>
            <td>
                {!! Form::open(['route' => ['facturaVentas.destroy', $facturaVenta->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('facturaVentas.show', [$facturaVenta->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('facturaVentas.edit', [$facturaVenta->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>