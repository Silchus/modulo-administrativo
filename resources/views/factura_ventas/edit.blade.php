@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Factura Venta
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($facturaVenta, ['route' => ['facturaVentas.update', $facturaVenta->id], 'method' => 'patch']) !!}

                        @include('factura_ventas.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection