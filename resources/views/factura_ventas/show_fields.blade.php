<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $facturaVenta->id !!}</p>
</div>

<!-- Fecha Field -->
<div class="form-group">
    {!! Form::label('fecha', 'Fecha:') !!}
    <p>{!! $facturaVenta->fecha !!}</p>
</div>

<!-- Numero Field -->
<div class="form-group">
    {!! Form::label('numero', 'Numero:') !!}
    <p>{!! $facturaVenta->numero !!}</p>
</div>

<!-- Codigo Field -->
<div class="form-group">
    {!! Form::label('codigo', 'Codigo:') !!}
    <p>{!! $facturaVenta->codigo !!}</p>
</div>

<!-- Cliente Id Field -->
<div class="form-group">
    {!! Form::label('cliente_id', 'Cliente Id:') !!}
    <p>{!! $facturaVenta->cliente_id !!}</p>
</div>

<!-- Vendedor Id Field -->
<div class="form-group">
    {!! Form::label('vendedor_id', 'Vendedor Id:') !!}
    <p>{!! $facturaVenta->vendedor_id !!}</p>
</div>

<!-- Forma1 Field -->
<div class="form-group">
    {!! Form::label('forma1', 'Forma1:') !!}
    <p>{!! $facturaVenta->forma1 !!}</p>
</div>

<!-- Forma2 Field -->
<div class="form-group">
    {!! Form::label('forma2', 'Forma2:') !!}
    <p>{!! $facturaVenta->forma2 !!}</p>
</div>

<!-- Descu Field -->
<div class="form-group">
    {!! Form::label('descu', 'Descu:') !!}
    <p>{!! $facturaVenta->descu !!}</p>
</div>

<!-- Totaldescu Field -->
<div class="form-group">
    {!! Form::label('totalDescu', 'Totaldescu:') !!}
    <p>{!! $facturaVenta->totalDescu !!}</p>
</div>

<!-- Iva Id Field -->
<div class="form-group">
    {!! Form::label('iva_id', 'Iva Id:') !!}
    <p>{!! $facturaVenta->iva_id !!}</p>
</div>

<!-- Totalsiva Field -->
<div class="form-group">
    {!! Form::label('totalSIva', 'Totalsiva:') !!}
    <p>{!! $facturaVenta->totalSIva !!}</p>
</div>

<!-- Exento Field -->
<div class="form-group">
    {!! Form::label('exento', 'Exento:') !!}
    <p>{!! $facturaVenta->exento !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $facturaVenta->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $facturaVenta->updated_at !!}</p>
</div>

