<!-- Fecha Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fecha', 'Fecha:') !!}
    {!! Form::date('fecha', null, ['class' => 'form-control','id'=>'fecha']) !!}
</div>

@section('scripts')
    <script type="text/javascript">
        $('#fecha').datetimepicker({
            format: 'YYYY-MM-DD',
            useCurrent: false
        })
    </script>
@endsection

<!-- Numero Field -->
<div class="form-group col-sm-6">
    {!! Form::label('numero', 'Numero:') !!}
    {!! Form::text('numero', null, ['class' => 'form-control']) !!}
</div>

<!-- Codigo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('codigo', 'Codigo:') !!}
    {!! Form::text('codigo', null, ['class' => 'form-control']) !!}
</div>

<!-- Cliente Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cliente_id', 'Cliente Id:') !!}
    {!! Form::select('cliente_id', [], null, ['class' => 'form-control']) !!}
</div>

<!-- Vendedor Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('vendedor_id', 'Vendedor Id:') !!}
    {!! Form::select('vendedor_id', [], null, ['class' => 'form-control']) !!}
</div>

<!-- Forma1 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('forma1', 'Forma1:') !!}
    {!! Form::text('forma1', null, ['class' => 'form-control']) !!}
</div>

<!-- Forma2 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('forma2', 'Forma2:') !!}
    {!! Form::text('forma2', null, ['class' => 'form-control']) !!}
</div>

<!-- Descu Field -->
<div class="form-group col-sm-6">
    {!! Form::label('descu', 'Descu:') !!}
    {!! Form::text('descu', null, ['class' => 'form-control']) !!}
</div>

<!-- Iva Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('iva_id', 'Iva Id:') !!}
    {!! Form::select('iva_id', [], null, ['class' => 'form-control']) !!}
</div>

<!-- Totalsiva Field -->
<div class="form-group col-sm-6">
    {!! Form::label('totalSIva', 'Totalsiva:') !!}
    {!! Form::text('totalSIva', null, ['class' => 'form-control']) !!}
</div>

<!-- Exento Field -->
<div class="form-group col-sm-6">
    {!! Form::label('exento', 'Exento:') !!}
    {!! Form::text('exento', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('facturaVentas.index') !!}" class="btn btn-default">Cancel</a>
</div>
