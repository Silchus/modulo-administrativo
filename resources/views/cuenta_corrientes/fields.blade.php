<!-- Codigo Field -->
<div class="form-group col-sm-3">
    {!! Form::label('codigo', 'Codigo:') !!}
    {!! Form::text('codigo', null, ['class' => 'form-control']) !!}
</div>

<!-- Fecha Field -->
<div class="form-group col-sm-3">
    {!! Form::label('fecha', 'Fecha:') !!}
    {!! Form::date('fecha', null, ['class' => 'form-control','id'=>'fecha']) !!}
</div>

@section('scripts')
    <script type="text/javascript">
        $('#fecha').datetimepicker({
            format: 'YYYY-MM-DD',
            useCurrent: false
        })
    </script>
@endsection

<!-- Cantidad Field -->
<div class="form-group col-sm-3">
    {!! Form::label('cantidad', 'Cantidad:') !!}
    {!! Form::text('cantidad', null, ['class' => 'form-control']) !!}
</div>

<!-- Importe Field -->
<div class="form-group col-sm-3">
    {!! Form::label('importe', 'Importe:') !!}
    {!! Form::text('importe', null, ['class' => 'form-control']) !!}
</div>

<!-- Tipo Field -->
<div class="form-group col-sm-3">
    {!! Form::label('tipo', 'Tipo:') !!}
    {!! Form::text('tipo', null, ['class' => 'form-control']) !!}
</div>

<!-- Costo Field -->
<div class="form-group col-sm-3">
    {!! Form::label('costo', 'Costo:') !!}
    {!! Form::text('costo', null, ['class' => 'form-control']) !!}
</div>

<!-- Articulo Id Field -->
<div class="form-group col-sm-3">
    {!! Form::label('articulo_id', 'Articulo Id:') !!}
    {!! Form::select('articulo_id', [], null, ['class' => 'form-control']) !!}
</div>

<!-- Saldo Field -->
<div class="form-group col-sm-3">
    {!! Form::label('saldo', 'Saldo:') !!}
    {!! Form::text('saldo', null, ['class' => 'form-control']) !!}
</div>

<!-- Ultimopago Field -->
<div class="form-group col-sm-3">
    {!! Form::label('ultimoPago', 'Ultimopago:') !!}
    {!! Form::text('ultimoPago', null, ['class' => 'form-control']) !!}
</div>

<!-- Ultimacompra Field -->
<div class="form-group col-sm-3">
    {!! Form::label('ultimaCompra', 'Ultimacompra:') !!}
    {!! Form::text('ultimaCompra', null, ['class' => 'form-control']) !!}
</div>

<!-- Cliente Id Field -->
<div class="form-group col-sm-3">
    {!! Form::label('cliente_id', 'Cliente Id:') !!}
    {!! Form::select('cliente_id', [], null, ['class' => 'form-control']) !!}
</div>

<!-- Recibo Field -->
<div class="form-group col-sm-3">
    {!! Form::label('recibo', 'Recibo:') !!}
    {!! Form::text('recibo', null, ['class' => 'form-control']) !!}
</div>

<!-- Recibotex Field -->
<div class="form-group col-sm-3">
    {!! Form::label('reciboTex', 'Recibotex:') !!}
    {!! Form::text('reciboTex', null, ['class' => 'form-control']) !!}
</div>

<!-- Costoreal Field -->
<div class="form-group col-sm-3">
    {!! Form::label('costoReal', 'Costoreal:') !!}
    {!! Form::text('costoReal', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('cuentaCorrientes.index') !!}" class="btn btn-default">Cancelar</a>
</div>
