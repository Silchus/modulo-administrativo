<table class="table table-responsive" id="cuentaCorrientes-table">
    <thead>
        <tr>
        <th>Fecha</th>
        <th>Cliente</th>
        <th>Descripcion</th>
        <th>Importe</th>
        <th>Saldo</th>
        <th>Ultimo pago</th>
        <th>Ultima compra</th>
         <th colspan="3">Acciones</th>
        </tr>
    </thead>
    <tbody>
    @foreach($cuentaCorrientes as $cuentaCorriente)
        <tr>
            <td>{!! $cuentaCorriente->fecha !!}</td>
            <td>{!! $cuentaCorriente->cliente_id !!}</td>
            <td>{!! $cuentaCorriente->descripcion !!}</td>
            <td>{!! $cuentaCorriente->importe !!}</td>
            <td>{!! $cuentaCorriente->saldo !!}</td>
            <td>{!! $cuentaCorriente->ultimoPago !!}</td>
            <td>{!! $cuentaCorriente->ultimaCompra !!}</td>
            <td>
                {!! Form::open(['route' => ['cuentaCorrientes.destroy', $cuentaCorriente->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('cuentaCorrientes.show', [$cuentaCorriente->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('cuentaCorrientes.edit', [$cuentaCorriente->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Está Seguro?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>