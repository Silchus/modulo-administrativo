@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Cuenta Corriente
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($cuentaCorriente, ['route' => ['cuentaCorrientes.update', $cuentaCorriente->id], 'method' => 'patch']) !!}

                        @include('cuenta_corrientes.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection