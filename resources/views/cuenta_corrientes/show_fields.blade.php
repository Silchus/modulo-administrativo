<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $cuentaCorriente->id !!}</p>
</div>

<!-- Codigo Field -->
<div class="form-group">
    {!! Form::label('codigo', 'Codigo:') !!}
    <p>{!! $cuentaCorriente->codigo !!}</p>
</div>

<!-- Fecha Field -->
<div class="form-group">
    {!! Form::label('fecha', 'Fecha:') !!}
    <p>{!! $cuentaCorriente->fecha !!}</p>
</div>

<!-- Descripcion Field -->
<div class="form-group">
    {!! Form::label('descripcion', 'Descripcion:') !!}
    <p>{!! $cuentaCorriente->descripcion !!}</p>
</div>

<!-- Cantidad Field -->
<div class="form-group">
    {!! Form::label('cantidad', 'Cantidad:') !!}
    <p>{!! $cuentaCorriente->cantidad !!}</p>
</div>

<!-- Importe Field -->
<div class="form-group">
    {!! Form::label('importe', 'Importe:') !!}
    <p>{!! $cuentaCorriente->importe !!}</p>
</div>

<!-- Tipo Field -->
<div class="form-group">
    {!! Form::label('tipo', 'Tipo:') !!}
    <p>{!! $cuentaCorriente->tipo !!}</p>
</div>

<!-- Costo Field -->
<div class="form-group">
    {!! Form::label('costo', 'Costo:') !!}
    <p>{!! $cuentaCorriente->costo !!}</p>
</div>

<!-- Articulo Id Field -->
<div class="form-group">
    {!! Form::label('articulo_id', 'Articulo Id:') !!}
    <p>{!! $cuentaCorriente->articulo_id !!}</p>
</div>

<!-- Saldo Field -->
<div class="form-group">
    {!! Form::label('saldo', 'Saldo:') !!}
    <p>{!! $cuentaCorriente->saldo !!}</p>
</div>

<!-- Ultimopago Field -->
<div class="form-group">
    {!! Form::label('ultimoPago', 'Ultimopago:') !!}
    <p>{!! $cuentaCorriente->ultimoPago !!}</p>
</div>

<!-- Ultimacompra Field -->
<div class="form-group">
    {!! Form::label('ultimaCompra', 'Ultimacompra:') !!}
    <p>{!! $cuentaCorriente->ultimaCompra !!}</p>
</div>

<!-- Cliente Id Field -->
<div class="form-group">
    {!! Form::label('cliente_id', 'Cliente Id:') !!}
    <p>{!! $cuentaCorriente->cliente_id !!}</p>
</div>

<!-- Recibo Field -->
<div class="form-group">
    {!! Form::label('recibo', 'Recibo:') !!}
    <p>{!! $cuentaCorriente->recibo !!}</p>
</div>

<!-- Recibotex Field -->
<div class="form-group">
    {!! Form::label('reciboTex', 'Recibotex:') !!}
    <p>{!! $cuentaCorriente->reciboTex !!}</p>
</div>

<!-- Costoreal Field -->
<div class="form-group">
    {!! Form::label('costoReal', 'Costoreal:') !!}
    <p>{!! $cuentaCorriente->costoReal !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $cuentaCorriente->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $cuentaCorriente->updated_at !!}</p>
</div>

