@extends('layouts.app')

@section('content')

    <section class="content-header">
        <div class="row">
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3>(cant)</h3>

                        <p>Nuevo Artículo</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-clipboard"></i>
                    </div>
                    <a href="{!! route('articulos.create') !!}" class="small-box-footer">Agregar <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3>(Cant)</h3>

                        <p>Nueva Categoria</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-window-restore"></i>
                    </div>

                    <a href="{!! route('tipoArticulos.create') !!}" class="small-box-footer">Agregar <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3>(Cant)</h3>

                        <p>Nueva Venta</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-truck"></i>
                    </div>
                    <a href="{!! route('ventas.create') !!}" class="small-box-footer">Agregar <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-red">
                    <div class="inner">
                        <h3>(cant)</h3>

                        <p>Nueva Compra</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-shopping-cart"></i>
                    </div>
                    <a href="/customer/view" class="small-box-footer">Agregar <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
        </div>

        <h1 class="pull-left">Articulos</h1>
      <!--  <h1 class="pull-right">
           <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('articulos.create') !!}">Add New</a>
        </h1>-->
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                    @include('articulos.table')
            </div>
        </div>
        <div class="text-center">
        
        </div>
    </div>
@endsection

