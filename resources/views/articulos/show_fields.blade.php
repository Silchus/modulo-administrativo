<!-- Id Field -->
<!--<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $articulo->id !!}</p>
</div>-->

<!-- Codigo Field -->
<div class="form-group col-sm-3">
    {!! Form::label('codigo', 'Codigo:') !!}
    <p>{!! $articulo->codigo !!}</p>
</div>

<!-- Descripcion Field -->
<div class="form-group col-sm-3">
    {!! Form::label('descripcion', 'Descripcion:') !!}
    <p>{!! $articulo->descripcion !!}</p>
</div>

<!-- Marca Field -->
<div class="form-group col-sm-3">
    {!! Form::label('marca', 'Marca:') !!}
    <p>{!! $articulo->marca !!}</p>

</div>

<!-- Stock Field -->
<div class="form-group col-sm-3">
    {!! Form::label('stock', 'Stock:') !!}
    <p>{!! $articulo->stock !!}</p>
</div>

<!-- Cantidad Field -->
<div class="form-group col-sm-3">
    {!! Form::label('cantidad', 'Cantidad:') !!}
    <p>{!! $articulo->cantidad !!}</p>
</div>

<!-- Stockminimo Field -->
<div class="form-group col-sm-3">
    {!! Form::label('stockMinimo', 'Stockminimo:') !!}
    <p>{!! $articulo->stockMinimo !!}</p>
</div>

<!-- Fechavencimiento Field -->
<div class="form-group col-sm-3">
    {!! Form::label('fechaVencimiento', 'Fechavencimiento:') !!}
    <p>{!! $articulo->fechaVencimiento !!}</p>
</div>

<!-- Ubicacion Field -->
<div class="form-group col-sm-3">
    {!! Form::label('ubicacion', 'Ubicacion:') !!}
    <p>{!! $articulo->ubicacion !!}</p>
</div>

<!-- Precio1 Field -->
<div class="form-group col-sm-3">
    {!! Form::label('precio1', 'Precio1:') !!}
    <p>{!! $articulo->precio1 !!}</p>
</div>

<!-- Precio2 Field -->
<div class="form-group col-sm-3">
    {!! Form::label('precio2', 'Precio2:') !!}
    <p>{!! $articulo->precio2 !!}</p>
</div>

<!-- Precio3 Field -->
<div class="form-group col-sm-3">
    {!! Form::label('precio3', 'Precio3:') !!}
    <p>{!! $articulo->precio3 !!}</p>
</div>

<!-- Costo Field -->
<div class="form-group col-sm-3">
    {!! Form::label('costo', 'Costo:') !!}
    <p>{!! $articulo->costo !!}</p>
</div>

<!-- Porcentajeganancia1 Field -->
<div class="form-group col-sm-3">
    {!! Form::label('porcentajeGanancia1', 'Porcentajeganancia1:') !!}
    <p>{!! $articulo->porcentajeGanancia1 !!}</p>
</div>

<!-- Porcentajeganancia2 Field -->
<div class="form-group col-sm-3">
    {!! Form::label('porcentajeGanancia2', 'Porcentajeganancia2:') !!}
    <p>{!! $articulo->porcentajeGanancia2 !!}</p>
</div>

<!-- Porcentajeganancia3 Field -->
<div class="form-group col-sm-3">
    {!! Form::label('porcentajeGanancia3', 'Porcentajeganancia3:') !!}
    <p>{!! $articulo->porcentajeGanancia3 !!}</p>
</div>

<!-- Precio Field -->
<!--<div class="form-group">
    {!! Form::label('precio', 'Precio:') !!}
    <p>{!! $articulo->precio !!}</p>
</div>
-->
<!-- Iva Field -->
<div class="form-group col-sm-3">
    {!! Form::label('iva', 'Iva:') !!}
    <p>{!! $articulo->iva !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group col-sm-3">
    {!! Form::label('created_at', 'Creado:') !!}
    <p>{!! $articulo->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group col-sm-3">
    {!! Form::label('updated_at', 'Actualizado:') !!}
    <p>{!! $articulo->updated_at !!}</p>
</div>

