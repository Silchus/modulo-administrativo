<!-- Codigo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('codigo', 'Codigo:') !!}
    {!! Form::text('codigo', null, ['class' => 'form-control']) !!}
    @if($errors->has('codigo'))
        <span class="invalid-feedback" role="alert" style="color: red; font-size: 9px">
            <strong>{{$errors->first('codigo')}}</strong>
        </span>
    @endif
</div>

<!-- Marca Field -->
<div class="form-group col-sm-6">
    {!! Form::label('marca', 'Marca:') !!}
    {!! Form::text('marca', null, ['class' => 'form-control']) !!}
    @if($errors->has('marca'))
        <span class="invalid-feedback" role="alert" style="color: red; font-size: 9px">
            <strong>{{$errors->first('marca')}}</strong>
        </span>
    @endif
</div>

<!-- Stock Field -->
<div class="form-group col-sm-4">
    {!! Form::label('stock', 'Stock:') !!}
    {!! Form::text('stock', null, ['class' => 'form-control']) !!}
    @if($errors->has('stock'))
        <span class="invalid-feedback" role="alert" style="color: red; font-size: 9px">
            <strong>{{$errors->first('stock')}}</strong>
        </span>
    @endif
</div>

<!-- Cantidad Field -->
<div class="form-group col-sm-4">
    {!! Form::label('cantidad', 'Cantidad:') !!}
    {!! Form::text('cantidad', null, ['class' => 'form-control']) !!}
    @if($errors->has('cantidad'))
        <span class="invalid-feedback" role="alert" style="color: red; font-size: 9px">
            <strong>{{$errors->first('cantidad')}}</strong>
        </span>
    @endif
</div>

<!-- Stockminimo Field -->
<div class="form-group col-sm-4">
    {!! Form::label('stockMinimo', 'Stock Mínimo:') !!}
    {!! Form::text('stockMinimo', null, ['class' => 'form-control']) !!}
    @if($errors->has('stockMinimo'))
        <span class="invalid-feedback" role="alert" style="color: red; font-size: 9px">
            <strong>{{$errors->first('stockMinimo')}}</strong>
        </span>
    @endif
</div>

<!-- Fechavencimiento Field -->
<div class="form-group col-sm-4">
    {!! Form::label('fechaVencimiento', 'Fecha vencimiento:') !!}
    {!! Form::date('fechaVencimiento', null, ['class' => 'form-control','id'=>'fechaVencimiento']) !!}
</div>

@section('scripts')
    <script type="text/javascript">
        $('#fechaVencimiento').datetimepicker({
            format: 'YYYY-MM-DD',
            useCurrent: false
        })
    </script>
@endsection

<!-- Ubicacion Field -->
<div class="form-group col-sm-4">
    {!! Form::label('ubicacion', 'Ubicación:') !!}
    {!! Form::text('ubicacion', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-4">
</div>


<div class="form-group col-sm-6">
</div>
<!-- Costo Field -->
<div class="form-group col-sm-2">
    {!! Form::label('costo', 'Costo:') !!}
    {!! Form::text('costo', null, ['class' => 'form-control']) !!}
    @if($errors->has('costo'))
        <span class="invalid-feedback" role="alert" style="color: red; font-size: 9px">
            <strong>{{$errors->first('costo')}}</strong>
        </span>
    @endif

</div>

<!-- Precio1 Field -->
<div class="form-group col-sm-2">
    {!! Form::label('precio1', 'Precio1:') !!}
    {!! Form::text('precio1', null, ['class' => 'form-control']) !!}
    @if($errors->has('Precio1'))
        <span class="invalid-feedback" role="alert" style="color: red; font-size: 9px">
            <strong>{{$errors->first('Precio1')}}</strong>
        </span>
    @endif

</div>


<!-- Porcentajeganancia1 Field -->
<div class="form-group col-sm-2">
    {!! Form::label('porcentajeGanancia1', 'Pje. Ganancia 1:') !!}
    {!! Form::text('porcentajeGanancia1', null, ['class' => 'form-control']) !!}
    @if($errors->has('porcentajeGanancia1'))
        <span class="invalid-feedback" role="alert"  style="color: red; font-size: 9px">
            <strong>{{$errors->first('porcentajeGanancia1')}}</strong>
        </span>
    @endif

</div>

<!-- Precio Field -->
<!--<div class="form-group col-sm-2">
    {!! Form::label('precio', 'Precio:') !!}
    {!! Form::text('precio', null, ['class' => 'form-control']) !!}
</div>-->

<div class="form-group col-sm-8">
</div>
<!-- Precio2 Field -->
<div class="form-group col-sm-2">
    {!! Form::label('precio2', 'Precio2:') !!}
    {!! Form::text('precio2', null, ['class' => 'form-control']) !!}
</div>

<!-- Porcentajeganancia2 Field -->
<div class="form-group col-sm-2">
    {!! Form::label('porcentajeGanancia2', 'Pje. Ganancia 2:') !!}
    {!! Form::text('porcentajeGanancia2', null, ['class' => 'form-control']) !!}
</div>


<div class="form-group col-sm-8">
</div>
<!-- Precio3 Field -->
<div class="form-group col-sm-2">
    {!! Form::label('precio3', 'Precio3:') !!}
    {!! Form::text('precio3', null, ['class' => 'form-control']) !!}
</div>

<!-- Porcentajeganancia3 Field -->
<div class="form-group col-sm-2">
    {!! Form::label('porcentajeGanancia3', 'Pje. Ganancia 3:') !!}
    {!! Form::text('porcentajeGanancia3', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-10">
</div>

<!-- Iva Field -->
<div class="form-group col-sm-2">
    {!! Form::label('iva', 'Iva:') !!}
    {!! Form::text('iva', null, ['class' => 'form-control']) !!}
</div>

<!-- Descripcion Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('descripcion', 'Descripción:') !!}
    {!! Form::textarea('descripcion', null, ['class' => 'form-control']) !!}
    @if($errors->has('porcentajeGanancia1'))
        <span class="invalid-feedback" role="alert"  style="color: red; font-size: 9px">
            <strong>{{$errors->first('descripcion')}}</strong>
        </span>
    @endif
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('articulos.index') !!}" class="btn btn-default">Cancelar</a>
</div>


