<!-- Tipo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tipo', 'Tipo:') !!}
    {!! Form::text('tipo', null, ['class' => 'form-control']) !!}
    @if($errors->has('tipo'))
        <span class="invalid-feedback" role="alert" style="color: red; font-size: 9px">
            <strong>{{$errors->first('tipo')}}</strong>
        </span>
    @endif
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('tipoArticulos.index') !!}" class="btn btn-default">Cancelar</a>
</div>
