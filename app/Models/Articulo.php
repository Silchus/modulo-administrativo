<?php

namespace App\Models;

//use Eloquent as Model;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Articulo
 * @package App\Models
 * @version April 28, 2019, 6:00 pm -03
 *
 * @property string codigo
 * @property string descripcion
 * @property string marca
 * @property integer stock
 * @property integer cantidad
 * @property integer stockMinimo
 * @property string fechaVencimiento
 * @property string ubicacion
 * @property float precio1
 * @property float precio2
 * @property float precio3
 * @property float costo
 * @property string porcentajeGanancia1
 * @property string porcentajeGanancia2
 * @property string porcentajeGanancia3
 * @property float precio
 * @property float iva
 */
class Articulo extends Model
{
    use SoftDeletes;

    public $table = 'articulos';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'codigo',
        'descripcion',
        'marca',
        'stock',
        'cantidad',
        'stockMinimo',
        'fechaVencimiento',
        'ubicacion',
        'precio1',
        'precio2',
        'precio3',
        'costo',
        'porcentajeGanancia1',
        'porcentajeGanancia2',
        'porcentajeGanancia3',
        'precio',
        'iva'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'codigo' => 'string',
        'descripcion' => 'string',
        'marca' => 'string',
        'stock' => 'integer',
        'cantidad' => 'integer',
        'stockMinimo' => 'integer',
        'fechaVencimiento' => 'date',
        'ubicacion' => 'string',
        'precio1' => 'float',
        'precio2' => 'float',
        'precio3' => 'float',
        'costo' => 'float',
        'porcentajeGanancia1' => 'string',
        'porcentajeGanancia2' => 'string',
        'porcentajeGanancia3' => 'string',
        'precio' => 'float',
        'iva' => 'float'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public function venta()
    {
        return $this->hasOne('App\Models\Venta');
    }

    public function categoria()
    {
        return $this->hasOne('App\Models\TipoArticulo','id','tipoArticulo_id')->select(array('id', 'tipo'));
    }

    public function proveedor()
    {
        return $this->hasOne('App\Models\Proveedor','id','proveedor_id')->select(array('id', 'empresa'));
    }

}
