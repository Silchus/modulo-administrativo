<?php

namespace App\Models;

//use Eloquent as Model;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Proveedor
 * @package App\Models
 * @version April 28, 2019, 10:54 am -03
 *
 * @property string nombre
 * @property string empresa
 * @property string direccion
 * @property string telefono
 * @property integer localidad_id
 * @property integer provincia_id
 * @property string cuit
 * @property string email
 * @property string otros
 * @property float saldo
 */
class Proveedor extends Model
{
    use SoftDeletes;

    public $table = 'proveedores';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'nombre',
        'empresa',
        'direccion',
        'telefono',
        'localidad_id',
        'provincia_id',
        'cuit',
        'email',
        'otros',
        'saldo'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nombre' => 'string',
        'empresa' => 'string',
        'direccion' => 'string',
        'telefono' => 'string',
        'localidad_id' => 'integer',
        'provincia_id' => 'integer',
        'cuit' => 'string',
        'email' => 'string',
        'otros' => 'string',
        'saldo' => 'float'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public function localidad()
    {
      return $this->hasOne('App\Models\Localidad', 'id', 'localidad_id');
    }

     public function provincia()
    {
      return $this->hasOne('App\Models\Provincia', 'id', 'provincia_id');
    }
    
}
