<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Socio extends Model{
    public $timestamps = false;
    protected $table = 'socio';
    protected $primaryKey = 'idSocio';

    public $fillable = ['apellido', 'nombre', 'mail', 'pword', 'codLesaro', 'cantMasc', 'idNegocio', 'dni', 'sexo', 'fechaNac', 'idLocalidad', 'direccion', 'nroDirec', 'piso', 'dpto', 'telefono', 'obser'];
    public function getRouteKeyName(){
        return 'idSocio';
    }

    public function mascotas(){ //UNA MUCHAS
        return $this->hasMany('App\Mascota', 'idMascota', 'idSocio');
    }

    public function negocio(){
        return $this->belongsToMany('App\Negocio', 'sociosXcliente', 'idCliente', 'idSocio');
    }

    public function localidad(){
        return $this->belongsTo('App\Localidad', 'idLocalidad', 'idSocio');
    }


    public function ventas(){
        return $this->hasOne('App\Models\Venta');
    }

    public function cuentaCorriente(){
        return $this->hasOne('App\Models\CuentaCorriente');
    }


}


