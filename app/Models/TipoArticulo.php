<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class TipoArticulo
 * @package App\Models
 * @version May 8, 2019, 9:17 am -03
 *
 * @property string tipo
 */
class TipoArticulo extends Model
{
    use SoftDeletes;

    public $table = 'tipo_articulos';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'tipo'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'tipo' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function articulo(){

        return $this->hasMany('App\Models\Articulo','tipoArticulo_id','id');
    }
}
