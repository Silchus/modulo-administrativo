<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class FacturaVenta
 * @package App\Models
 * @version May 15, 2019, 5:36 pm -03
 *
 * @property string fecha
 * @property integer numero
 * @property string codigo
 * @property integer cliente_id
 * @property integer vendedor_id
 * @property string forma1
 * @property string forma2
 * @property float descu
 * @property float totalDescu
 * @property integer iva_id
 * @property float totalSIva
 * @property float exento
 */
class FacturaVenta extends Model
{
    use SoftDeletes;

    public $table = 'factura_ventas';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'fecha',
        'numero',
        'codigo',
        'cliente_id',
        'vendedor_id',
        'forma1',
        'forma2',
        'descu',
        'totalDescu',
        'iva_id',
        'totalSIva',
        'exento'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'fecha' => 'date',
        'numero' => 'integer',
        'codigo' => 'string',
        'cliente_id' => 'integer',
        'vendedor_id' => 'integer',
        'forma1' => 'string',
        'forma2' => 'string',
        'descu' => 'float',
        'totalDescu' => 'float',
        'iva_id' => 'integer',
        'totalSIva' => 'float',
        'exento' => 'float'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
