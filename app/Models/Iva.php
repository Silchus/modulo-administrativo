<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Iva
 * @package App\Models
 * @version May 8, 2019, 9:20 am -03
 *
 * @property float iva
 * @property string descripcion
 */
class Iva extends Model
{
    use SoftDeletes;

    public $table = 'ivas';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'iva',
        'descripcion'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'iva' => 'float',
        'descripcion' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
