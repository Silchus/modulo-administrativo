<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class CuentaCorriente
 * @package App\Models
 * @version May 8, 2019, 9:42 am -03
 *
 * @property string codigo
 * @property string fecha
 * @property string descripcion
 * @property integer cantidad
 * @property float importe
 * @property string tipo
 * @property float costo
 * @property integer articulo_id
 * @property float saldo
 * @property float ultimoPago
 * @property float ultimaCompra
 * @property integer cliente_id
 * @property integer recibo
 * @property string reciboTex
 * @property float costoReal
 */
class CuentaCorriente extends Model
{
    use SoftDeletes;

    public $table = 'cuenta_corrientes';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'codigo',
        'fecha',
        'descripcion',
        'cantidad',
        'importe',
        'tipo',
        'costo',
        'articulo_id',
        'saldo',
        'ultimoPago',
        'ultimaCompra',
        'cliente_id',
        'recibo',
        'reciboTex',
        'costoReal'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'codigo' => 'string',
        'fecha' => 'date',
        'descripcion' => 'string',
        'cantidad' => 'integer',
        'importe' => 'float',
        'tipo' => 'string',
        'costo' => 'float',
        'articulo_id' => 'integer',
        'saldo' => 'float',
        'ultimoPago' => 'float',
        'ultimaCompra' => 'float',
        'cliente_id' => 'integer',
        'recibo' => 'integer',
        'reciboTex' => 'string',
        'costoReal' => 'float'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function Socio(){
        return $this->belongsTo('App\Models\Socio');
    }
    
}
