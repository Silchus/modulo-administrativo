<?php

namespace App\Models;

//use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Venta
 * @package App\Models
 * @version May 4, 2019, 11:13 am -03
 *
 * @property string fecha
 * @property string descripcion
 * @property integer cantidad
 * @property float importe
 * @property string cajaDiaria
 * @property integer articulo_id
 * @property float costo
 * @property integer cliente_id
 * @property string vendedor
 * @property string usuario
 * @property string forma1
 * @property string forma2
 * @property integer facturaVenta_id
 * @property float iva
 */
class Venta extends Model
{
    use SoftDeletes;

    public $table = 'ventas';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'fecha',
        'descripcion',
        'cantidad',
        'importe',
        'cajaDiaria',
        'articulo_id',
        'costo',
        'cliente_id',
        'vendedor',
        'usuario',
        'forma1',
        'forma2',
        'facturaVenta_id',
        'iva'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'fecha' => 'date',
        'descripcion' => 'string',
        'cantidad' => 'integer',
        'importe' => 'float',
        'cajaDiaria' => 'string',
        'articulo_id' => 'integer',
        'costo' => 'float',
        'cliente_id' => 'integer',
        'vendedor' => 'string',
        'usuario' => 'string',
        'forma1' => 'string',
        'forma2' => 'string',
        'facturaVenta_id' => 'integer',
        'iva' => 'float'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function articulos()
    {
        return $this->hasOne('App\Models\Articulo','artculo_id','id')->select(array('articulo_id', 'descripcion'));
    }

    public function categoria()
    {
        return $this->hasOne('App\Models\TipoArticulo','id','tipoArticulo_id')->select(array('id', 'tipo'));
    }

    public function socio()
    {
        return $this->hasOne('App\Models\Socio','cliente_id','idSocio')->select(array('id', 'nombre'));
    }



}
