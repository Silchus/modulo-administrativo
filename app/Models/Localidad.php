<?php

namespace App\Models;

//use Eloquent as Model;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Localidad
 * @package App\Models
 * @version November 13, 2018, 8:35 pm UTC
 *
 * @property string nombre
 * @property string codigoPostal
 * @property integer provincia_id
 */
class Localidad extends Model
{
    use SoftDeletes;

    public $table = 'localidades';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'nombre',
        'codigoPostal',
        'provincia_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'nombre' => 'string',
        'codigoPostal' => 'string',
        'provincia_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nombre' => 'required',
        'codigoPostal' => 'required',
        'provincia_id' => 'required'
    ];

}
