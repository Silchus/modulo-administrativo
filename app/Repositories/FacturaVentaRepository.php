<?php

namespace App\Repositories;

use App\Models\FacturaVenta;
use App\Repositories\BaseRepository;

/**
 * Class FacturaVentaRepository
 * @package App\Repositories
 * @version May 15, 2019, 5:36 pm -03
*/

class FacturaVentaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'fecha',
        'numero',
        'codigo',
        'cliente_id',
        'vendedor_id',
        'forma1',
        'forma2',
        'descu',
        'totalDescu',
        'iva_id',
        'totalSIva',
        'exento'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return FacturaVenta::class;
    }
}
