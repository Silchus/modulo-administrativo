<?php

namespace App\Repositories;

use App\Models\CuentaCorriente;
use App\Repositories\BaseRepository;

/**
 * Class CuentaCorrienteRepository
 * @package App\Repositories
 * @version May 8, 2019, 9:42 am -03
*/

class CuentaCorrienteRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'codigo',
        'fecha',
        'descripcion',
        'cantidad',
        'importe',
        'tipo',
        'costo',
        'articulo_id',
        'saldo',
        'ultimoPago',
        'ultimaCompra',
        'cliente_id',
        'recibo',
        'reciboTex',
        'costoReal'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CuentaCorriente::class;
    }
}
