<?php

namespace App\Repositories;

use App\Models\Venta;
use App\Repositories\BaseRepository;

/**
 * Class VentaRepository
 * @package App\Repositories
 * @version May 4, 2019, 11:13 am -03
*/

class VentaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'fecha',
        'descripcion',
        'cantidad',
        'importe',
        'cajaDiaria',
        'articulo_id',
        'costo',
        'cliente_id',
        'vendedor',
        'usuario',
        'forma1',
        'forma2',
        'facturaVenta_id',
        'iva'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Venta::class;
    }
}
