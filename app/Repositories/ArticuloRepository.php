<?php

namespace App\Repositories;

use App\Models\Articulo;
use App\Repositories\BaseRepository;

/**
 * Class ArticuloRepository
 * @package App\Repositories
 * @version April 28, 2019, 6:00 pm -03
*/

class ArticuloRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'codigo',
        'descripcion',
        'marca',
        'stock',
        'cantidad',
        'stockMinimo',
        'fechaVencimiento',
        'ubicacion',
        'precio1',
        'precio2',
        'precio3',
        'costo',
        'porcentajeGanancia1',
        'porcentajeGanancia2',
        'porcentajeGanancia3',
        'precio',
        'iva'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Articulo::class;
    }
}
