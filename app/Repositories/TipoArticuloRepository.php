<?php

namespace App\Repositories;

use App\Models\TipoArticulo;
use App\Repositories\BaseRepository;

/**
 * Class TipoArticuloRepository
 * @package App\Repositories
 * @version May 8, 2019, 9:17 am -03
*/

class TipoArticuloRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'tipo'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TipoArticulo::class;
    }
}
