<?php

namespace App\Repositories;

use App\Models\Iva;
use App\Repositories\BaseRepository;

/**
 * Class IvaRepository
 * @package App\Repositories
 * @version May 8, 2019, 9:20 am -03
*/

class IvaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'iva',
        'descripcion'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Iva::class;
    }
}
