<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCuentaCorrienteRequest;
use App\Http\Requests\UpdateCuentaCorrienteRequest;
use App\Repositories\CuentaCorrienteRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class CuentaCorrienteController extends AppBaseController
{
    /** @var  CuentaCorrienteRepository */
    private $cuentaCorrienteRepository;

    public function __construct(CuentaCorrienteRepository $cuentaCorrienteRepo)
    {
        $this->cuentaCorrienteRepository = $cuentaCorrienteRepo;
    }

    /**
     * Display a listing of the CuentaCorriente.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $cuentaCorrientes = $this->cuentaCorrienteRepository->all();

        return view('cuenta_corrientes.index')
            ->with('cuentaCorrientes', $cuentaCorrientes);
    }

    /**
     * Show the form for creating a new CuentaCorriente.
     *
     * @return Response
     */
    public function create()
    {
        return view('cuenta_corrientes.create');
    }

    /**
     * Store a newly created CuentaCorriente in storage.
     *
     * @param CreateCuentaCorrienteRequest $request
     *
     * @return Response
     */
    public function store(CreateCuentaCorrienteRequest $request)
    {
        $input = $request->all();

        $cuentaCorriente = $this->cuentaCorrienteRepository->create($input);

        Flash::success('Cuenta Corriente saved successfully.');

        return redirect(route('cuentaCorrientes.index'));
    }

    /**
     * Display the specified CuentaCorriente.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $cuentaCorriente = $this->cuentaCorrienteRepository->find($id);

        if (empty($cuentaCorriente)) {
            Flash::error('Cuenta Corriente not found');

            return redirect(route('cuentaCorrientes.index'));
        }

        return view('cuenta_corrientes.show')->with('cuentaCorriente', $cuentaCorriente);
    }

    /**
     * Show the form for editing the specified CuentaCorriente.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $cuentaCorriente = $this->cuentaCorrienteRepository->find($id);

        if (empty($cuentaCorriente)) {
            Flash::error('Cuenta Corriente not found');

            return redirect(route('cuentaCorrientes.index'));
        }

        return view('cuenta_corrientes.edit')->with('cuentaCorriente', $cuentaCorriente);
    }

    /**
     * Update the specified CuentaCorriente in storage.
     *
     * @param int $id
     * @param UpdateCuentaCorrienteRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCuentaCorrienteRequest $request)
    {
        $cuentaCorriente = $this->cuentaCorrienteRepository->find($id);

        if (empty($cuentaCorriente)) {
            Flash::error('Cuenta Corriente not found');

            return redirect(route('cuentaCorrientes.index'));
        }

        $cuentaCorriente = $this->cuentaCorrienteRepository->update($request->all(), $id);

        Flash::success('Cuenta Corriente updated successfully.');

        return redirect(route('cuentaCorrientes.index'));
    }

    /**
     * Remove the specified CuentaCorriente from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $cuentaCorriente = $this->cuentaCorrienteRepository->find($id);

        if (empty($cuentaCorriente)) {
            Flash::error('Cuenta Corriente not found');

            return redirect(route('cuentaCorrientes.index'));
        }

        $this->cuentaCorrienteRepository->delete($id);

        Flash::success('Cuenta Corriente deleted successfully.');

        return redirect(route('cuentaCorrientes.index'));
    }

    //Valida los datos que se ingresan en el formulario
    private function validarDatos(Request $request, $id = null){

        return $validator = Validator::make($request->all(),
            [
                'codigo' => 'string|unique:articulos',
                'fecha' => 'required|date|date_format:Y-m-d',
                'cliente' => 'required|string',
                'descripcion' => 'required|string',
                'cantidad' => 'required|string',
                'importe' => 'numeric',
                'stock' => 'required|string',
                'stockMinimo' => 'required|string',
                'ubicacion' => 'string',
                'costo' => 'string',
            ]);
    }
}
