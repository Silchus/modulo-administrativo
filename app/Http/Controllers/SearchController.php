<?php

namespace App\Http\Controllers;

use Carbon\Traits\Date;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use Eloquent as Model;
use App\Models\TipoArticulo;
use App\Models\Venta;
use App\Models\Articulo;
use App\Models\Proveedor;
use Illuminate\Database\Eloquent\SoftDeletes;


class SearchController extends Controller
{
    public function tipoArticulo_nombre(Request $request){

		$term = $request['term'];

		$results = array();

		$queries = TipoArticulo::where('tipo', 'LIKE', '%'.$term.'%')->with('articulos')->take(3)->get();
       /* $queries = \DB::table('tipo_articulos')
            ->where('tipo', 'LIKE', '%'.$term.'%')
            ->take(3)->get();*/


		foreach ($queries as $key => $value)
		{
			$queries[$key]['value'] = $value->tipo;
		}

		return \Response::json($queries);
	}

	//public function purchase_category_name(Request $request){
	public function compras_categoria_nombre(Request $request){

       // var_dump($request); exit;
		$term = $request['term'];

		$results = array();

        $queries = Articulo::where('descripcion', 'LIKE', '%'.$term.'%')
                ->take(3)->get();

        $data = array();
        foreach ($queries as $key => $value)
        {

            $data[$key]['value'] = $value->descripcion .'('.$value->marca.')';
            $data[$key]['precio_compra'] = $value->costo;
            $data[$key]['precio_venta'] = $value->precio1;
            $data[$key]['opening_stock'] = $value->cantidad;
            $data[$key]['fecha'] = '2019-05-18';

        }

        //dd($data);
		/*$data = array();

        foreach ($queries as $key => $value)
		{

			$data[$key]['id'] = $value->id;
			$data[$key]['value'] = $value->tipo;
            $cant = 0;
            while ($cant < count($value->articulo)) {
                //dd($value->articulo[0]);
                $val1 = $value->articulo[$cant];
            if($data[$cant]['articulos'][$val1->articulo_id]['dimention'] = null)
                $data[$cant]['articulos'][$val1->articulo_id]['dimention'] = null;

                $data[$cant]['articulos'][$val1->articulo_id]['precio_compra'] = $val1->costo;
                $data[$cant]['articulos'][$val1->articulo_id]['precio_venta'] = $val1->precio1;
                $data[$cant]['articulos'][$val1->articulo_id]['opening_stock'] = $val1->cantidad;
               // $data[$cant]['articulos'][$val1->articulo_id]['costo'] = $val1->costo;
                $data[$cant]['articulos'][$val1->articulo_id]['fecha'] = '2019-05-18';

				//foreach ($val1->articulo as $key2 => $val2) {
                if($data[$cant]['articulos'][$val1->articulo_id]['dimention'] == 0){
                    $data[$cant]['articulos'][$val1->articulo_id]['title'] = $val1->descripcion;
                    $data[$cant]['articulos'][$val1->articulo_id]['dimention'] = $cant;
                }
                else{
                    $data[$cant]['articulos'][$val1->articulo_id]['title'] = $data[$cant]['articulos'][$val1->articulo_id]['title'].' x '.$val1->descripcion;
                    $data[$cant]['articulos'][$val1->articulo_id]['dimention'] = $data[$cant]['articulos'][$val1->articulo_id]['dimention'].' x '.$cant;
                }
                $cant++;
			}
            //dd($data);

        }*/

		return \Response::json($data);
	}

	//public function supplier_name(Request $request){
	public function proveedor_nombre(Request $request){

		$term = $request['term'];
		
		$results = array();
		
		$queries = Proveedor::where('nombre', 'LIKE', '%'.$term.'%')->take(5)->get();

		foreach ($queries as $key => $value)
		{
			$queries[$key]['value'] = $value->nombre;
		}
		
		return \Response::json($queries);
	}

	//public function stock_name(Request $request){
	public function articulo_nombre(Request $request){

		$term = $request['term'];
		
		$results = array();
		
		$queries = \DB::table('articulos')
                    ->where('descripcion', 'LIKE', '%'.$term.'%')->take(5)->get();

		foreach ($queries as $key => $value)
		{
			$queries[$key]['value'] = $value->descripcion;
		}
		
		return \Response::json($queries);
	}

	public function socio_nombre(Request $request){

        $term = $request['term'];

        $results = array();

        $queries = \DB::table('socio')
            ->join('cuenta_corrientes', 'cuenta_corrientes.cliente_id', '=', 'socio.idSocio')
            ->where('nombre', 'LIKE', '%'.$term.'%')
            ->orWhere('apellido', 'LIKE', '%'.$term.'%')
            ->take(5)->get();

        foreach ($queries as $query)
        {
            $results[] = [ 'value' => $query->nombre.' '.$query->apellido,
                            'socio_direccion'=> $query->direccion,
                            'socio_telefono'=> $query->telefono,
                            'saldo'=> $query->saldo]
        ;
        }
        return \Response::json($results);

	}
}
