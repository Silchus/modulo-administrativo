<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateVentaRequest;
use App\Http\Requests\UpdateVentaRequest;
use App\Models\Venta;
use App\Repositories\VentaRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;
use Validator;

use Eloquent as Model;

use App\Models\Articulo;
use App\Models\Socio;

class VentaController extends AppBaseController
{
    /** @var  VentaRepository */
    private $ventaRepository;

    public function __construct(VentaRepository $ventaRepo)
    {
        $this->ventaRepository = $ventaRepo;
    }

    /**
     * Display a listing of the Venta.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $ventas = $this->ventaRepository->all();

        return view('ventas.index')
            ->with('ventas', $ventas);

        if(!isset($request['order'])){
            if($request['order']!='desc')
                $request['order']='desc';

        }

        if(!isset($request['search'])){
            if($request['search']!='%%')
                $request['search']='%%';
        }else{
            $request['search']='%'.$request['search'].'%';
        }

        if(!isset($request['filter'])){

            $request['filter'] = [];

        }

        if(!isset($request['sort'])){
            if($request['sort']!='id')
                $request['sort']='id';

        }

        $rows = Venta::where(function($query) use($request){
            $query->orwhere('id','like',$request['search'])
                ->orwhere('nombre','like',$request['search'])
                ->orwhere('apellido','like',$request['search']);
            // agregar ->with('categoria') cuando este creada, despues de  ->with('articulo')->with('usuarios')->
        })->with('articulo')->with('socio')->with('categoria')->orderBy($request['sort'],$request['order'])
            ->skip($request['offset'])
            ->take($request['limit'])
            ->get();

        $total = Venta::where(function($query) use($request){
            $query->orwhere('id','like',$request['search'])
                ->orwhere('nombre','like',$request['search'])
                ->orwhere('apellido','like',$request['search']);
        })->count();

        return ['rows'=>$rows,'total'=>$total];
    }

    /**
     * Show the form for creating a new Venta.
     *
     * @return Response
     */
    public function create()
    {
        return view('ventas.create');
    }

    /**
     * Store a newly created Venta in storage.
     *
     * @param CreateVentaRequest $request
     *
     * @return Response
     */
    public function store(CreateVentaRequest $request)
    {

        $validator = $this->validarDatos($request);
var_dump($validator); exit;
        if($validator->fails())
            return redirect(Route('ventas.create'))
                ->withErrors($validator->errors())
                ->withInput();
        else{
            $data = $request->all();
            var_dump($data); exit;

            $articulos = array();

            unset($request['tipoArticulo_nombre'],
            $request['tipoArticulo_id'],
            $request['articulo_id'],
            $request['costo'],
            $request['selling_cost'],
            $request['opening_stock'],
            $request['closing_stock'],
            $request['cantidad']);

            $venta = Venta::create($request->all());

            foreach ($data['articulo_id'] as $key => $value) {

                $articulos[$key]['id'] = $venta->id;
                $articulos[$key]['articulo_id'] = $value;
                // $articulos[$key]['tipoArticulo_nombre'] = $data['tipoArticulo_nombre'][$key];
                $articulos[$key]['tipoArticulo_id'] = $data['tipoArticulo_id'][$key];
                $articulos[$key]['costo'] = $data['costo'][$key];
                $articulos[$key]['precio1'] = $data['precio_venta'][$key];
                $articulos[$key]['opening_stock'] = $data['opening_stock'][$key];
                $articulos[$key]['closing_stock'] = $data['closing_stock'][$key];
                $articulos[$key]['cantidad'] = $data['cantidad'][$key];
                $articulos[$key]['sub_total'] = $data['sub_total'][$key];
            }

            $ventas = Venta::insert($articulos);

            foreach ($articulos as $key => $value) {

                \App\Models\Articulo::where('id',$value['id'])
                    ->update(['cantidad'=>$value['closing_stock']]);
            }

            $SalesDetail = Socio::where('id',$request['idSocio'])
                ->join('cuenta_corrientes', 'cuenta_corrientes.cliente_id', '=', 'socio.idSocio')
                ->update([
                    // 'balance'=>$request['closing_balance'],
                    'cuenta_corrientes.saldo'=>$request['saldo']
                ]);

            /*      $transaction_details = [
                      'type' => 2,
                      'sales_id'=>$SalesDetail->sales_id,
                      'customer_id'=>$SalesDetail->customer_id,
                      'subtotal'=>$SalesDetail->grand_total,

                      'payment'=>$SalesDetail->payment,
                      'balance'=>$SalesDetail->closing_balance,
                      'due'=>$SalesDetail->closing_due,
                      'mode'=>$SalesDetail->mode,
                  ];

                  \App\Models\Transaction::create($transaction_details);
          */
            $messageType = 1;
            $message = "Sales created successfully !";
            try {

            } catch(\Illuminate\Database\QueryException $ex){
                $messageType = 2;
                $message = "Error al generar una nueva venta!";
            }

            return redirect(url("/ventas/view"))->with('messageType',$messageType)->with('message',$message);

        }
    }

    /**
     * Display the specified Venta.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $venta = $this->ventaRepository->find($id);

        if (empty($venta)) {
            Flash::error('Venta not found');

            return redirect(route('ventas.index'));
        }

        return view('ventas.show')->with('venta', $venta);
    }

    /**
     * Show the form for editing the specified Venta.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $venta = $this->ventaRepository->find($id);

        if (empty($venta)) {
            Flash::error('Venta not found');

            return redirect(route('ventas.index'));
        }

        $ventas = Venta::find($id);

        return view('ventas.edit')->with('venta', $venta);
    }

    /**
     * Update the specified Venta in storage.
     *
     * @param int $id
     * @param UpdateVentaRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateVentaRequest $request)
    {
        $validator = $this->validarDatos($id,$request);

        if($validator->fails()){
            return redirect(Route('ventas.edit',$id))
                ->withErrors($validator->errors())
                ->withInput();
        }else{
            $venta = $this->ventaRepository->find($id);

            if (empty($venta)) {
                Flash::error('Venta not found');

                return redirect(route('ventas.index'));
            }

            $venta = $this->ventaRepository->update($request->all(), $id);

            Flash::success('Venta updated successfully.');

            return redirect(route('ventas.index'));
          }
    }

    /**
     * Remove the specified Venta from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $venta = $this->ventaRepository->find($id);

        if (empty($venta)) {
            Flash::error('Venta not found');

            return redirect(route('ventas.index'));
        }

        $this->ventaRepository->delete($id);

        Flash::success('Venta deleted successfully.');

        return redirect(route('ventas.index'));
    }


    //Valida los datos que se ingresan en el formulario
    private function validarDatos(Request $request, $id = null){

        return $validator = Validator::make($request->all(),
            [
                //'fecha' => 'date',
                'descripcion' => 'string',
                'cantidad' => 'required|numeric',
                'importe' => 'required|numeric',
                'costo' => 'numeric',
                'vendedor' => 'string',
                'usuario' => 'string',
                'forma1' => '',
                'forma2' => '',
                'iva' => '',
            ]);
    }
}
