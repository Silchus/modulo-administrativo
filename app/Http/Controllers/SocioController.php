<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SocioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(!isset($request['order'])){
            if($request['order']!='desc')
            $request['order']='desc';
        
        }

        if(!isset($request['search'])){
            if($request['search']!='%%')
            $request['search']='%%';
        
        }else{
            $request['search']='%'.$request['search'].'%';
        }

        if(!isset($request['filter'])){

            $request['filter'] = [];
        
        }

        if(!isset($request['sort'])){
            if($request['sort']!='id')
            $request['sort']='id';
        
        }

        $rows = \App\Models\Socio::where(function($query) use($request){
                      $query->orwhere('idSocio','like',$request['search'])
                      ->orwhere('nombre','like',$request['search'])
                      ->orwhere('mail','like',$request['search'])
                      ->orwhere('direccion','like',$request['search'])
                      ->orwhere('telefono','like',$request['search']);
                  })->orderBy($request['sort'],$request['order'])
                    ->skip($request['offset'])
                    ->take($request['limit'])
                    ->get();

        $total = \App\Models\Socio::where(function($query) use($request){
                      $query->orwhere('id','like',$request['search'])
                      ->orwhere('nombre','like',$request['search'])
                      ->orwhere('email','like',$request['search'])
                      ->orwhere('direccion','like',$request['search'])
                      ->orwhere('telefono','like',$request['search']);
                  })->count();

        return ['rows'=>$rows,'total'=>$total];
    }

    public function view()
    {
        return view('socio.view');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('socio.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            
            \App\Models\Socio::create($request->all());

            $messageType = 1;
            $message = "Socio created successfully !";

        } catch(\Illuminate\Database\QueryException $ex){  
            $messageType = 2;
            $message = "Socio creation failed !";
        }

        return redirect(url("/socio/view"))->with('messageType',$messageType)->with('message',$message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        dd($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $socio = \App\Models\Socio::find($id);

        return view('socio.edit')->with('socio',$socio);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {

            $socio = \App\Models\Socio::find($id);

            $socio->update($request->all());

            $messageType = 1;
            $message = "Socio ".$socio->name." details updated successfully !";

        } catch(\Illuminate\Database\QueryException $ex){  
            $messageType = 2;
            $message = "Socio updation failed !";
        }

        return redirect(url("/socio/view"))->with('messageType',$messageType)->with('message',$message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            
            $socio = \App\Models\Socio::find($id);

            $socio->delete();

            $messageType = 1;
            $message = "Socio ".$socio->nombre." details deleted successfully !";

        } catch(\Illuminate\Database\QueryException $ex){  
            $messageType = 2;
            $message = "Socio deletion failed !";
        }
        
        return redirect(url("/socio/view"))->with('messageType',$messageType)->with('message',$message);
    }
}
