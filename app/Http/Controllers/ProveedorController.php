<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateProveedorRequest;
use App\Http\Requests\UpdateProveedorRequest;
use App\Repositories\ProveedorRepository;
use App\Http\Controllers\AppBaseController;
use Validator;
use Illuminate\Http\Request;
use App\Models\Provincia;
use App\Models\Localidad;
use Flash;
use Illuminate\Routing\Route;
use Response;

class ProveedorController extends AppBaseController
{
    /** @var  ProveedorRepository */
    private $proveedorRepository;

    public function __construct(ProveedorRepository $proveedorRepo)
    {
        $this->proveedorRepository = $proveedorRepo;
    }

    /**
     * Display a listing of the Proveedor.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $proveedores = $this->proveedorRepository->all();

        return view('proveedores.index')
            ->with('proveedores', $proveedores);
    }

    /**
     * Show the form for creating a new Proveedor.
     *
     * @return Response
     */
    public function create()
    {
        $provincias = Provincia::pluck('nombre', 'id')->toArray();
        $localidades = Localidad::pluck('nombre', 'id')->toArray();

        return view('proveedores.create', ['localidades' => $localidades,
                                           'provincias' => $provincias
                                          ]);
    }

    /**
     * Store a newly created Proveedor in storage.
     *
     * @param CreateProveedorRequest $request
     *
     * @return Response
     */
    public function store(CreateProveedorRequest $request)
    {
        $input = $request->all();

        $validator = $this->validarDatos($request);
        
        if($validator->fails())
            return redirect(Route('proveedores.create'))
                        ->withErrors($validator->errors())
                        ->withInput();
        else{  

                $proveedor = $this->proveedorRepository->create($input);
                Flash::success('Los datos se guardaron correctamente');
                return redirect(route('proveedores.index'));
            }
    }

    /**
     * Display the specified Proveedor.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $proveedor = $this->proveedorRepository->find($id);

        if (empty($proveedor)) {
            Flash::error('Proveedor not found');

            return redirect(route('proveedores.index'));
        }

        return view('proveedores.show', [])->with('proveedor', $proveedor);
    }

    /**
     * Show the form for editing the specified Proveedor.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $proveedor = $this->proveedorRepository->find($id);
        $provincias = Provincia::pluck('nombre', 'id')->toArray();
        $localidades = Localidad::pluck('nombre', 'id')->toArray();

        if (empty($proveedor)) {
            Flash::error('Proveedor not found');

            return redirect(route('proveedores.index'));
        }

        return view('proveedores.edit', ['proveedor' => $proveedor,
                                         'localidades' => $localidades, 
                                         'provincias' => $provincias
            ]);
    }

    /**
     * Update the specified Proveedor in storage.
     *
     * @param int $id
     * @param UpdateProveedorRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProveedorRequest $request)
    {
        $validator = $this->validarDatos($id,$request);
        if($validator->fails()){
            return redirect(Route('proveedor.edit',$id))
                ->withErrors($validator->errors())
                ->withInput();
        }else{
            $proveedor = $this->proveedorRepository->find($id);

            if (empty($proveedor)) {
                Flash::error('Proveedor not found');

                return redirect(route('proveedores.index'));
            }

            $proveedor = $this->proveedorRepository->update($request->all(), $id);

            Flash::success('Proveedor updated successfully.');

            return redirect(route('proveedores.index'));
        }
    }

    /**
     * Remove the specified Proveedor from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $proveedor = $this->proveedorRepository->find($id);

        if (empty($proveedor)) {
            Flash::error('Proveedor not found');

            return redirect(route('proveedores.index'));
        }

        $this->proveedorRepository->delete($id);

        Flash::success('Proveedor deleted successfully.');

        return redirect(route('proveedores.index'));
    }

    //Valida los datos que se ingresan en el formulario
    private function validarDatos(Request $request, $id = null){

        return $validator = Validator::make($request->all(),
            [
                'nombre' => 'required|string',
                'empresa' => 'required|string',
                'direccion' => 'string',
                'telefono' => 'required|numeric',
                'email' => 'string',
                'cuit' => 'required|string|unique:proveedores',
                'provincia' => 'string',
                'localidad' => 'string',
                'saldo' => 'numeric',
                'otros' => 'string',
            ]);
    }
}
