<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateFacturaVentaRequest;
use App\Http\Requests\UpdateFacturaVentaRequest;
use App\Repositories\FacturaVentaRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class FacturaVentaController extends AppBaseController
{
    /** @var  FacturaVentaRepository */
    private $facturaVentaRepository;

    public function __construct(FacturaVentaRepository $facturaVentaRepo)
    {
        $this->facturaVentaRepository = $facturaVentaRepo;
    }

    /**
     * Display a listing of the FacturaVenta.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $facturaVentas = $this->facturaVentaRepository->all();

        return view('factura_ventas.index')
            ->with('facturaVentas', $facturaVentas);
    }

    /**
     * Show the form for creating a new FacturaVenta.
     *
     * @return Response
     */
    public function create()
    {
        return view('factura_ventas.create');
    }

    /**
     * Store a newly created FacturaVenta in storage.
     *
     * @param CreateFacturaVentaRequest $request
     *
     * @return Response
     */
    public function store(CreateFacturaVentaRequest $request)
    {
        $input = $request->all();

        $facturaVenta = $this->facturaVentaRepository->create($input);

        Flash::success('Factura Venta saved successfully.');

        return redirect(route('facturaVentas.index'));
    }

    /**
     * Display the specified FacturaVenta.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $facturaVenta = $this->facturaVentaRepository->find($id);

        if (empty($facturaVenta)) {
            Flash::error('Factura Venta not found');

            return redirect(route('facturaVentas.index'));
        }

        return view('factura_ventas.show')->with('facturaVenta', $facturaVenta);
    }

    /**
     * Show the form for editing the specified FacturaVenta.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $facturaVenta = $this->facturaVentaRepository->find($id);

        if (empty($facturaVenta)) {
            Flash::error('Factura Venta not found');

            return redirect(route('facturaVentas.index'));
        }

        return view('factura_ventas.edit')->with('facturaVenta', $facturaVenta);
    }

    /**
     * Update the specified FacturaVenta in storage.
     *
     * @param int $id
     * @param UpdateFacturaVentaRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateFacturaVentaRequest $request)
    {
        $facturaVenta = $this->facturaVentaRepository->find($id);

        if (empty($facturaVenta)) {
            Flash::error('Factura Venta not found');

            return redirect(route('facturaVentas.index'));
        }

        $facturaVenta = $this->facturaVentaRepository->update($request->all(), $id);

        Flash::success('Factura Venta updated successfully.');

        return redirect(route('facturaVentas.index'));
    }

    /**
     * Remove the specified FacturaVenta from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $facturaVenta = $this->facturaVentaRepository->find($id);

        if (empty($facturaVenta)) {
            Flash::error('Factura Venta not found');

            return redirect(route('facturaVentas.index'));
        }

        $this->facturaVentaRepository->delete($id);

        Flash::success('Factura Venta deleted successfully.');

        return redirect(route('facturaVentas.index'));
    }
}
