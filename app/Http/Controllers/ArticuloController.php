<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateArticuloRequest;
use App\Http\Requests\UpdateArticuloRequest;
use App\Repositories\ArticuloRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;
use Validator;

class ArticuloController extends AppBaseController
{
    /** @var  ArticuloRepository */
    private $articuloRepository;

    public function __construct(ArticuloRepository $articuloRepo)
    {
        $this->articuloRepository = $articuloRepo;
    }

    /**
     * Display a listing of the Articulo.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $articulos = $this->articuloRepository->all();

        return view('articulos.index')
            ->with('articulos', $articulos);
    }

    /**
     * Show the form for creating a new Articulo.
     *
     * @return Response
     */
    public function create()
    {
        return view('articulos.create');
    }

    /**
     * Store a newly created Articulo in storage.
     *
     * @param CreateArticuloRequest $request
     *
     * @return Response
     */
    public function store(CreateArticuloRequest $request)
    {
        $input = $request->all();

        $validator = $this->validarDatos($request);

        if($validator->fails())
            return redirect(Route('articulos.create'))
                ->withErrors($validator->errors())
                ->withInput();
        else{
            $articulo = $this->articuloRepository->create($input);
            Flash::success('Articulo saved successfully.');
            return redirect(route('articulos.index'));
        }




    }

    /**
     * Display the specified Articulo.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $articulo = $this->articuloRepository->find($id);

        if (empty($articulo)) {
            Flash::error('Articulo not found');

            return redirect(route('articulos.index'));
        }

        return view('articulos.show')->with('articulo', $articulo);
    }

    /**
     * Show the form for editing the specified Articulo.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $articulo = $this->articuloRepository->find($id);

        if (empty($articulo)) {
            Flash::error('Articulo not found');

            return redirect(route('articulos.index'));
        }

        return view('articulos.edit')->with('articulo', $articulo);
    }

    /**
     * Update the specified Articulo in storage.
     *
     * @param int $id
     * @param UpdateArticuloRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateArticuloRequest $request)
    {
        $validator = $this->validarDatos($id,$request);
        if($validator->fails()){
            return redirect(Route('articulo.edit',$id))
                ->withErrors($validator->errors())
                ->withInput();
        }else{
            $articulo = $this->articuloRepository->find($id);

            if (empty($articulo)) {
                Flash::error('Articulo not found');

                return redirect(route('articulos.index'));
            }

            $articulo = $this->articuloRepository->update($request->all(), $id);

            Flash::success('Articulo updated successfully.');

            return redirect(route('articulos.index'));
        }
    }

    /**
     * Remove the specified Articulo from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $articulo = $this->articuloRepository->find($id);

        if (empty($articulo)) {
            Flash::error('Articulo not found');

            return redirect(route('articulos.index'));
        }

        $this->articuloRepository->delete($id);

        Flash::success('Articulo deleted successfully.');

        return redirect(route('articulos.index'));
    }

    //Valida los datos que se ingresan en el formulario
    private function validarDatos(Request $request, $id = null){

        return $validator = Validator::make($request->all(),
        [
           'codigo' => 'required|string|unique:articulos',
           'marca' => 'required|string',
           'descripcion' => 'required|string',
           'stock' => 'required|string',
           'cantidad' => 'required|string',
           'stockMinimo' => 'required|string',
           'fechaVencimiento' => 'date|date_format:Y-m-d',
           'ubicacion' => 'string',
           'costo' => 'string',
           'precio1' => 'required|numeric',
           'precio2' => 'numeric',
           'precio3' => 'numeric',
         //  'precio' => 'numeric|max:128',
           'porcentajeGanancia1' => 'required|numeric',
           'porcentajeGanancia2' => 'numeric',
           'porcentajeGanancia3' => 'numeric',
        ]);
    }
}
