<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::resource('proveedores', 'ProveedorController');

Route::resource('articulos', 'ArticuloController');

Route::resource('ventas', 'VentaController');

Route::resource('tipoArticulos', 'TipoArticuloController');

Route::resource('ivas', 'IvaController');

Route::resource('cuentaCorrientes', 'CuentaCorrienteController');

Route::resource('facturaVentas', 'FacturaVentaController');


Route::group(['prefix' => 'search'], function () {

    Route::any('/proveedor_nombre', 'SearchController@proveedor_nombre');
    Route::any('/articulo_nombre', 'SearchController@articulo_nombre');
    Route::any('/socio_nombre', 'SearchController@socio_nombre');
    Route::any('/tipoArticulo_nombre', 'SearchController@tipoArticulo_nombre');
    Route::any('/compras_categoria_nombre', 'SearchController@compras_categoria_nombre');

});